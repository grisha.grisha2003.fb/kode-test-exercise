﻿#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <math.h>
#include <algorithm>
#include <fstream>
#include <Windows.h>
#include <conio.h>
#include "ListObject.h"
#include "List.h"
#include "FileManager.h"

using namespace std;

HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE); // Получаем дескриптор консоли

void GoToXY(short x, short y)
{
    SetConsoleCursorPosition(hStdOut, { x, y });
}
void ConsoleCursorVisible(bool show, short size)
{
    CONSOLE_CURSOR_INFO structCursorInfo;
    GetConsoleCursorInfo(hStdOut, &structCursorInfo);
    structCursorInfo.bVisible = show;
    structCursorInfo.dwSize = size;
    SetConsoleCursorInfo(hStdOut, &structCursorInfo);
}

int main()
{
    setlocale(LC_ALL, "Russian");

    FileManager FManager;
    List list;
    //Переменные для ввода
    string temp = "";
    string temp2 = "";
    long double x = 0;
    long double y = 0;
    long double time = 0;





    // Инициализация интерфейса
    SetConsoleTitle(L"KODE Exercise");
    string Menu[] = { "Загрузить файл", "Добавить объект", "Отсортировать как:", "По имени", "По расстоянию", "По типу", "По времени" };
    int menuSize = 0;
    int active_menu = 0;
    char ch;

   
    while (true)
    {
        system("CLS");
        ConsoleCursorVisible(false, 100);
        GoToXY(0, 0);


        SetConsoleTextAttribute(hStdOut, WHITE_PEN);
        cout << "Управление: W/UP - вверх, S/Down - вниз, D/Right - выбрать, Enter - поддтвердить, Esc - выход. ";
        
        
        // Печать меню
        int x = 15, y = 5;
        GoToXY(x, y);
        int menuSize = size(Menu);
        for (int i = 0; i < menuSize; i++)
        {
            if (i == active_menu) SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
            else SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE);
            GoToXY(x, y+=2);
            cout << Menu[i] << endl;
            if (list.empty()) { menuSize = 1; break; }
        }

        // Ввод команд
        ch = _getch();
        if (ch == -32) ch = _getch();
        switch (ch)
        {
        case 27:
            exit(0); // Выход из программы
        case 119:
        case 87:
        case -106:
        case -26: 
        case 72:  // Вверх
            if (active_menu > 0) active_menu--;
            else active_menu = menuSize - 1;
            break;
        case 115:
        case 83:
        case -101:
        case -21:
        case 80:   // Вниз
            if (active_menu < menuSize - 1) active_menu++;
            else active_menu = 0;

            break;
        case 100:
        case 68:
        case -94:
        case -126:
        case 77:   // Вправо
            switch (active_menu)
            {
            case 0:
                GoToXY(30, 7); SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                cout << "Введите путь";
                ConsoleCursorVisible(true, 1);
                GoToXY(45, 7);
                cin >> temp; list = FManager.getListFromFile(temp);
                break;

            case 1:
                SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                system("CLS");
                GoToXY(15, 9); cout << "Имя"; GoToXY(15, 11); cout << "X"; GoToXY(15, 13); cout << "Y"; GoToXY(15, 15); cout << "Тип"; GoToXY(15, 17); cout << "Время";
                ConsoleCursorVisible(true, 1);
                GoToXY(25, 9); cin >> temp; GoToXY(25, 11); cin >> x; GoToXY(25, 13); cin >> y; GoToXY(25, 15); cin >> temp2; GoToXY(25, 17); cin >> time;
                list.push_back(ListObject(temp, x, y, temp2, time));                
                break;
            case 2:
                active_menu++;
                break;
            case 3:
                GoToXY(30, 13); SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                cout << "Введите путь";
                ConsoleCursorVisible(true, 1);
                GoToXY(45, 13);
                cin >> temp; FManager.outputListSortedByName(temp, list);
                break;
            case 4:
                GoToXY(30, 15); SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                cout << "Введите путь";
                ConsoleCursorVisible(true, 1);
                GoToXY(45, 15);
                cin >> temp; FManager.outputListSortedByDistance(temp, list);
                break;
            case 5:
                GoToXY(30, 17); SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                cout << "Введите путь";
                ConsoleCursorVisible(true, 1);
                GoToXY(45, 17);
                cin >> temp; FManager.outputListSortedByType(temp, list);
                break;
            case 6:
                GoToXY(30, 19); SetConsoleTextAttribute(hStdOut, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
                cout << "Введите путь";
                ConsoleCursorVisible(true, 1);
                GoToXY(45, 19);
                cin >> temp; FManager.outputListSortedByTime(temp, list);
                break;
                break;

            }
            break;
        }
    }

    
    return 0;
}


