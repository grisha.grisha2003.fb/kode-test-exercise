#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <math.h>
#include <algorithm>
#include <fstream>
using namespace std;



class ListObject
{
public:
    string Name;
    long double X;
    long double Y;
    string Type;
    time_t Time;

    ListObject();

    ListObject(string Name, long double X, long double Y, string Type, time_t Time);

    ListObject(string FullString);
        
private:

    string subdivideStringOnce(int& i, string& Fullstring);
};