#pragma once
#include "List.h"
#include "ListObject.h"
#include <unordered_map>
#include <time.h>

using namespace std;

class FileManager
{
public:

    List getListFromFile(string path);

    void outputListSortedByName(string path, List& list);
    void outputListSortedByDistance(string path, List& list);
    void outputListSortedByType(string path, List& list);
    void outputListSortedByTime(string path, List& list);

private:
    ifstream InputFileStream;
    ofstream OutputFileStream;
};