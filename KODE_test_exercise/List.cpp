#include "List.h"

void List::push_back(ListObject Object)
{
    MainList.push_back(Object);
}

void List::push_back(string Name, long double X, long double Y, string Type, time_t Time)
{
    ListObject L_Temp(Name, X, Y, Type, Time);
    MainList.push_back(L_Temp);
}

ListObject List::getByIndex(int Index)
{
    if (MainList.size() > Index) return MainList[Index];
    else return MainList.back();
}

int List::size() { return MainList.size(); }

bool List::empty() { return MainList.empty(); }

//����������
void List::sortByName()
{
    sort(MainList.begin(), MainList.end(), [](const ListObject& a, const ListObject& b)
        {
            return a.Name < b.Name;
        });
    // ��������� ��������������� ������ �������, ������������� � �����/�������. ��� ���� ������� � �������������� �������
    vector<ListObject> ListOfNumberNames; ListOfNumberNames.clear();
    int i = 0;
    for (i; i < MainList.size(); i++)
    {
        if (MainList[i].Name[0] >= 'A'/*����������*/) break;
        ListOfNumberNames.push_back(MainList[i]);
    };
    // ��������� ��������������� ������ ����������� �������. ��� ���� ����� ��������� � �������� �������
    vector<ListObject> ListOfEnNames; ListOfEnNames.clear();
    for (i; i < MainList.size(); i++)
    {
        if (int(MainList[i].Name[0]) < 0 /*�������*/) break;
        ListOfEnNames.push_back(MainList[i]);
    }
    // ��������� ��������������� ������ �������� �������. ��� ���� � ����� ��������� �������
    vector<ListObject> ListOfRuNames; ListOfRuNames.clear();
    for (i; i < MainList.size(); i++)
    {
        ListOfRuNames.push_back(MainList[i]);
    }
    // ����� ���������� ������������� ���������� � ������� ������� � ���������� �������
    
    //���������� ����������� �������
    if (!ListOfEnNames.empty() || ListOfEnNames.size() > 2) sort(ListOfEnNames.begin(), ListOfEnNames.end(), [](const ListObject& a, const ListObject& b)
        {
            string Alphabet = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
            int aCost = 0; int bCost = 0;
            for (int i = 0; i < a.Name.size() && i < b.Name.size(); i++)
            {
                for (int g = 0; g < Alphabet.size(); g++)
                {
                    if (a.Name[i] == Alphabet[g]) aCost = g;
                    if (b.Name[i] == Alphabet[g]) bCost = g;
                }
                if (aCost != bCost) break;
            }
            return aCost < bCost;
        });

    //���������� �������� �������
    if (!ListOfRuNames.empty() || ListOfRuNames.size() > 2) sort(ListOfRuNames.begin(), ListOfRuNames.end(), [](const ListObject& a, const ListObject& b)
        {
            string Alphabet = "�����������娸����������������������������������������������������";
            int aCost = 0; int bCost = 0;
            for (int i = 0; i < a.Name.size() && i < b.Name.size(); i++)
            {
                for (int g = 0; g < Alphabet.size(); g++)
                {
                    if (a.Name[i] == Alphabet[g]) aCost = g;
                    if (b.Name[i] == Alphabet[g]) bCost = g;
                }
                if (aCost != bCost) break;
            }
            return aCost < bCost;
        });
    //���������� ������� � ���������� �������
    MainList.clear();
    MainList.insert(MainList.begin(), ListOfRuNames.begin(), ListOfRuNames.end()); ListOfRuNames.clear();
    MainList.insert(MainList.end(), ListOfEnNames.begin(), ListOfEnNames.end()); ListOfEnNames.clear();
    MainList.insert(MainList.end(), ListOfNumberNames.begin(), ListOfNumberNames.end()); ListOfNumberNames.clear(); // ������� ������� ��� �������-�� ����������
}

void List::sortByType()
{
    this->sortByName();
    sort(MainList.begin(), MainList.end(), [](const ListObject& a, const ListObject& b)
        {
            return a.Type < b.Type;
        });
    // ��������� ��������������� ������ �������, ������������� � �����/�������. ��� ���� ������� � �������������� �������
    vector<ListObject> ListOfNumberNames; ListOfNumberNames.clear();
    int i = 0;
    for (i; i < MainList.size(); i++)
    {
        if (MainList[i].Type[0] >= 'A'/*����������*/) break;
        ListOfNumberNames.push_back(MainList[i]);
    };
    // ��������� ��������������� ������ ����������� �������. ��� ���� ����� ��������� � �������� �������
    vector<ListObject> ListOfEnNames; ListOfEnNames.clear();
    for (i; i < MainList.size(); i++)
    {
        if (int(MainList[i].Type[0]) < 0 /*�������*/) break;
        ListOfEnNames.push_back(MainList[i]);
    }
    // ��������� ��������������� ������ �������� �������. ��� ���� � ����� ��������� �������
    vector<ListObject> ListOfRuNames; ListOfRuNames.clear();
    for (i; i < MainList.size(); i++)
    {
        ListOfRuNames.push_back(MainList[i]);
    }
    // ����� ���������� ������������� ���������� � ������� ������� � ���������� �������

    //���������� ����������� �������
    if (!ListOfEnNames.empty() || ListOfEnNames.size() > 2) sort(ListOfEnNames.begin(), ListOfEnNames.end(), [](const ListObject& a, const ListObject& b)
        {
            string Alphabet = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
            int aCost = 0; int bCost = 0;
            for (int i = 0; i < a.Type.size() && i < b.Type.size(); i++)
            {
                for (int g = 0; g < Alphabet.size(); g++)
                {
                    if (a.Type[i] == Alphabet[g]) aCost = g;
                    if (b.Type[i] == Alphabet[g]) bCost = g;
                }
                if (aCost != bCost) break;
            }
            return aCost < bCost;
        });

    //���������� �������� �������
    if (!ListOfRuNames.empty() || ListOfRuNames.size() > 2) sort(ListOfRuNames.begin(), ListOfRuNames.end(), [](const ListObject& a, const ListObject& b)
        {
            string Alphabet = "�����������娸����������������������������������������������������";
            int aCost = 0; int bCost = 0;
            for (int i = 0; i < a.Type.size() && i < b.Type.size(); i++)
            {
                for (int g = 0; g < Alphabet.size(); g++)
                {
                    if (a.Type[i] == Alphabet[g]) aCost = g;
                    if (b.Type[i] == Alphabet[g]) bCost = g;
                }
                if (aCost != bCost) break;
            }
            return aCost < bCost;
        });
    //���������� ������� � ���������� �������
    MainList.clear();
    MainList.insert(MainList.begin(), ListOfRuNames.begin(), ListOfRuNames.end()); ListOfRuNames.clear();
    MainList.insert(MainList.end(), ListOfEnNames.begin(), ListOfEnNames.end()); ListOfEnNames.clear();
    MainList.insert(MainList.end(), ListOfNumberNames.begin(), ListOfNumberNames.end()); ListOfNumberNames.clear();
}

void List::sortByDistance()
{
    this->sortByName();
    sort(MainList.begin(), MainList.end(), [](const ListObject& a, const ListObject& b)
        {
            long double aDistance = sqrtl(pow(a.X, 2) + pow(a.Y, 2));
            long double bDistance = sqrtl(pow(b.X, 2) + pow(b.Y, 2));
            return aDistance < bDistance;
        });
}

void List::sortByTime()
{
    this->sortByName();
    sort(MainList.begin(), MainList.end(), [](const ListObject& a, const ListObject& b)
        {
            return a.Time < b.Time;
        });
}

