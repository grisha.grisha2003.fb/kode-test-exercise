#pragma once
#include "ListObject.h"
using namespace std;

class List
{
public:

    void push_back(ListObject Object);

    void push_back(string Name, long double X, long double Y, string Type, time_t Time);

    ListObject getByIndex(int Index);

    int size();

    bool empty();


    //Сортировки
    void sortByName();

    void sortByType();

    void sortByDistance();

    void sortByTime();


private:
    vector<ListObject> MainList;
};