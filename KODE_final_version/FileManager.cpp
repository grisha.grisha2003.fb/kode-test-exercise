#include "FileManager.h"


List FileManager::getListFromFile(string path)
{
    InputFileStream.open(path);
    if (!InputFileStream.is_open()) { List EmptyList; return EmptyList; } 

    string line;
    List outList;
    while (!InputFileStream.eof())
    {
        line = "";
        getline(InputFileStream, line);
        if(line != "") outList.push_back(ListObject(line));
    }
    InputFileStream.close();
    return outList;
}

void FileManager::outputListSortedByName(string path, List& list)
{
    list.sortByName();
    OutputFileStream.open(path);
    if (!OutputFileStream.is_open()) return;

    string Alphabet = "�����������娸����������������������������������������������������";
    char groupName = ' ';
    bool alphabetEnded = false;
    for (int i = 0; i < list.size(); i++)
    {
        ListObject tempLO = list.getByIndex(i);
        if (!alphabetEnded)
        {
            for (int j = 0; j < Alphabet.size() - 1; j += 2)
            {
                if (tempLO.Name[0] == Alphabet[j] || tempLO.Name[0] == Alphabet[j + 1])
                {
                    if (groupName != Alphabet[j])
                    {
                        if (groupName == ' ')
                        {
                            groupName = Alphabet[j];
                            OutputFileStream << "������ " << groupName << ":" << endl;
                            break;
                        }
                        else
                        {
                            groupName = Alphabet[j];
                            OutputFileStream << endl << "������ " << groupName << ":" << endl;
                            break;
                        }
                    }
                }
                if (int(tempLO.Name[0]) > -1)
                {
                    if (groupName == ' ')
                    {
                        OutputFileStream << "������ #:" << endl;;
                        alphabetEnded = true;
                        break;
                    }
                    else
                    {
                        OutputFileStream << endl << "������ #:" << endl;;
                        alphabetEnded = true;
                        break;

                    }
                }
            }
        }
        OutputFileStream << tempLO.Name << " " << tempLO.X << " " << tempLO.Y << " " << tempLO.Type << " " << tempLO.Time << endl;
    }
    OutputFileStream.close();
}

void FileManager::outputListSortedByDistance(string path, List& list)
{
    list.sortByDistance();
    OutputFileStream.open(path);
    if (!OutputFileStream.is_open()) return;


    string groupName = "";
    bool tooFar = false;
    for (int i = 0; i < list.size(); i++)
    {
        ListObject tempLO = list.getByIndex(i);

        if (!tooFar)
        {
            long double distance = sqrtl(pow(tempLO.X, 2) + pow(tempLO.Y, 2));

            if (distance < 100.0)
            {
                if (groupName != "�� 100 ��.")
                {
            
                    groupName = "�� 100 ��.";
                    OutputFileStream << groupName << endl;
                }
            }
            if (distance > 100.0 && distance < 1000.0)
            {
                if (groupName != "�� 1000 ��.")
                {
                    if (groupName == "")
                    {
                        groupName = "�� 1000 ��.";
                        OutputFileStream << groupName << endl;
                    }
                    else
                    {
                        groupName = "�� 1000 ��.";
                        OutputFileStream << endl << groupName << endl;
                    }
                }
            }
            if (distance > 1000.0 && distance < 10000.0)
            {
                if (groupName != "�� 10000 ��.")
                {
                    if (groupName == "")
                    {
                        groupName = "�� 10000 ��.";
                        OutputFileStream << groupName << endl;
                    }
                    else
                    {
                        groupName = "�� 10000 ��.";
                        OutputFileStream << endl << groupName << endl;
                    }
                }
            }
            if (distance > 10000.0) { tooFar = true;  OutputFileStream << endl << "������� ������." << endl; }
        }
        OutputFileStream << tempLO.Name << " " << tempLO.X << " " << tempLO.Y << " " << tempLO.Type << " " << tempLO.Time << endl;
    }
    OutputFileStream.close();
}

void FileManager::outputListSortedByType(string path, List& list)
{
    list.sortByType();
    OutputFileStream.open(path);
    if (!OutputFileStream.is_open()) return;

    unordered_map<string, List> map;
    //��������� �������
    for (int i = 0; i < list.size(); i++)
    {
        map[list.getByIndex(i).Type].push_back(list.getByIndex(i));
    }
    //������� �������, ������� ������ 2�
    for (auto& pair : map)
    {
        if (pair.second.size() >= 2)
        {
            OutputFileStream << pair.first << endl;

            for (int i = 0; i < pair.second.size(); i++)
            {
                ListObject tempLO = pair.second.getByIndex(i);
                OutputFileStream << tempLO.Name << " " << tempLO.X << " " << tempLO.Y << " " << tempLO.Type << " " << tempLO.Time << endl;
            }
        }
    }
    if (!map.empty())
    {
        OutputFileStream << "������" << endl;
        for (auto& pair : map)
        {
            if (pair.second.size() == 1)
            {
                ListObject tempLO = pair.second.getByIndex(0);
                OutputFileStream << tempLO.Name << " " << tempLO.X << " " << tempLO.Y << " " << tempLO.Type << " " << tempLO.Time << endl;
            }
        }
    }
    map.clear();
    OutputFileStream.close();
}

void FileManager::outputListSortedByTime(string path, List& list)
{
    list.sortByTime();
    OutputFileStream.open(path);
    if (!OutputFileStream.is_open()) return;

    string groupName = "";
    bool Previously = false;
    //�������� ������� ����
    time_t currentTime = time(0);
    struct tm currentDate;
    localtime_s(&currentDate, &currentTime);

    for (int i = 0; i < list.size(); i++)
    {
        ListObject tempLO = list.getByIndex(i);
        // �������� ����� � ���� �������
        time_t objectTime = ceil(tempLO.Time);
        struct tm objectDate;
        localtime_s(&objectDate, &objectTime);

        if (!Previously)
        {
            if (objectDate.tm_year == currentDate.tm_year)
            {
                if (objectDate.tm_mon == currentDate.tm_mon)
                {
                    if (objectDate.tm_yday / 7 == objectDate.tm_yday / 7)
                    {
                        if (objectDate.tm_mday < currentDate.tm_mday - 1)
                        {
                            if (objectDate.tm_mday == currentDate.tm_mday)
                            {
                                if (groupName != "�������") { groupName = "�������"; OutputFileStream << groupName << endl; }
                            }
                            else
                            {
                                if (groupName != "�����") { groupName = "�����"; OutputFileStream << groupName << endl; }
                            }
                        }
                        else
                        {
                            if (groupName != "�� ���� ������") { groupName = "�� ��� ������"; OutputFileStream << groupName << endl; }
                        }
                    }
                    else
                    {
                        if (groupName != "� ���� ������") { groupName = "� ���� ������"; OutputFileStream << groupName << endl; }
                    }
                }
                else
                {
                    if (groupName != "� ���� ����") { groupName = "� ���� ����"; OutputFileStream << groupName << endl; }
                }
            }
            else
            {
                OutputFileStream << "�����" << endl;
                Previously = true;
            }
        }
        OutputFileStream << tempLO.Name << " " << tempLO.X << " " << tempLO.Y << " " << tempLO.Type << " " << tempLO.Time << endl;
    }

    OutputFileStream.close();
}

